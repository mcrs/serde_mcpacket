# serde_mcpacket

A serde serializer/deserializer for Minecraft packets on protocol version 47 (game version 1.8.9).

## License

Licensed under [CNPLv6+](https://gitlab.com/elise/serde_mcpacket/-/blob/main/LICENSE.md)
