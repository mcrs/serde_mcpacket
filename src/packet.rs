use std::io::{Cursor, Read};

use flate2::{read::ZlibDecoder, Compression};
use serde::{de::DeserializeOwned, Serialize};
use tokio::io::AsyncReadExt;

use crate::{
	de::{DeserializerError, PacketDeserializer},
	ser::{PacketSerializer, SerializerError},
	varlen::{VarInt, VarLen},
};

pub trait PacketInfo: Serialize + DeserializeOwned {
	const PACKET_ID: i32;
}

#[derive(Debug, Clone, PartialEq)]
pub struct RawPacket {
	pub packet_id: i32,
	pub data: Vec<u8>,
}

impl RawPacket {
	pub async fn from_async_read<R: tokio::io::AsyncRead + Send + Sync + Unpin>(
		reader: &mut R,
		compressed: bool,
	) -> Result<Self, DeserializerError> {
		let total_packet_length =
			VarInt::from_async_reader(reader).await.map_err(DeserializerError::IoError)?;
		let next_varint =
			VarInt::from_async_reader(reader).await.map_err(DeserializerError::IoError)?;
		if total_packet_length.0 < next_varint.count_bytes() {
			return Err(DeserializerError::InvalidDataLength);
		}

		let (packet_id, data) = if compressed {
			let data_length_vi = next_varint;

			let data_length = total_packet_length.0 - data_length_vi.count_bytes();

			match data_length_vi.0 {
				1..=i32::MAX => {
					let mut compressed: Vec<u8> = vec![0u8; data_length as usize];
					reader.read_exact(&mut compressed).await.map_err(DeserializerError::IoError)?;

					let decompressed_length = data_length_vi.0;

					let mut decoder = ZlibDecoder::new(Cursor::new(compressed));
					let mut decompressed = vec![0u8; decompressed_length as usize];
					decoder.read_exact(&mut decompressed).map_err(DeserializerError::IoError)?;

					let packet_id = VarInt::from_bytes(&decompressed);
					let data = decompressed[packet_id.count_bytes() as usize..].to_vec();

					(packet_id, data)
				}
				0 => {
					let packet_id = VarInt::from_async_reader(reader)
						.await
						.map_err(DeserializerError::IoError)?;
					let mut data = vec![0u8; (data_length - packet_id.count_bytes()) as usize];
					reader.read_exact(&mut data).await.map_err(DeserializerError::IoError)?;

					(packet_id, data)
				}
				_ => {
					return Err(DeserializerError::InvalidDataLength);
				}
			}
		} else {
			let packet_id = next_varint;

			let mut data = vec![0u8; (total_packet_length.0 - packet_id.count_bytes()) as usize];
			reader.read_exact(&mut data).await.map_err(DeserializerError::IoError)?;

			(packet_id, data)
		};

		Ok(Self { packet_id: packet_id.0, data })
	}

	pub fn from_read<R: std::io::Read + Send + Sync + Unpin>(
		reader: &mut R,
		compression_threshold: Option<i32>,
	) -> Result<Self, DeserializerError> {
		let total_packet_length =
			VarInt::from_reader(reader).map_err(DeserializerError::IoError)?;
		let next_varint = VarInt::from_reader(reader).map_err(DeserializerError::IoError)?;
		if total_packet_length.0 < next_varint.count_bytes() {
			return Err(DeserializerError::InvalidDataLength);
		}

		let (packet_id, data) = if compression_threshold.is_some() {
			let data_length_vi = next_varint;

			let compressed_data_length = total_packet_length.0 - data_length_vi.count_bytes();

			match data_length_vi.0 {
				1..=i32::MAX => {
					let mut decompressed: Vec<u8> = Vec::new();
					let mut decoder = ZlibDecoder::new(reader);
					decoder.read_to_end(&mut decompressed).map_err(DeserializerError::IoError)?;

					if decompressed.len() != data_length_vi.0 as usize {
						panic!(
							"decompressed.len() != data_length_vi.0 as usize ({} != {})",
							decompressed.len(),
							data_length_vi.0 as usize
						)
					}

					let packet_id = VarInt::from_bytes(&decompressed);
					let data = decompressed[packet_id.count_bytes() as usize..].to_vec();

					(packet_id, data)
				}
				0 => {
					let packet_id =
						VarInt::from_reader(reader).map_err(DeserializerError::IoError)?;
					let mut data =
						vec![0u8; (compressed_data_length - packet_id.count_bytes()) as usize];
					reader.read_exact(&mut data).map_err(DeserializerError::IoError)?;

					(packet_id, data)
				}
				_ => {
					return Err(DeserializerError::InvalidDataLength);
				}
			}
		} else {
			let packet_id = next_varint;

			let mut data = vec![0u8; (total_packet_length.0 - packet_id.count_bytes()) as usize];
			reader.read_exact(&mut data).map_err(DeserializerError::IoError)?;

			(packet_id, data)
		};

		Ok(Self { packet_id: packet_id.0, data })
	}

	pub fn finalize_compressed(&self, threshold: i32) -> Result<Vec<u8>, SerializerError> {
		let packet_id = self.packet_id;
		let raw = self.finalize_raw(packet_id);
		let raw_len = raw.len();

		if threshold.is_positive() && raw_len >= threshold as usize {
			log::info!("Compressed and over threshold");

			let uncompressed_len =
				raw_len.try_into().map_err(|_| SerializerError::PacketTooLarge(raw_len))?;
			let uncompressed_len = VarInt(uncompressed_len);

			let compressed_data = {
				let mut encoder =
					flate2::bufread::ZlibEncoder::new(raw.as_slice(), Compression::default());
				let mut data = Vec::new();
				encoder.read_to_end(&mut data).map_err(SerializerError::Compression)?;
				data
			};

			let packet_len = compressed_data.len() + uncompressed_len.count_bytes() as usize;
			let packet_len = VarInt(
				packet_len.try_into().map_err(|_| SerializerError::PacketTooLarge(raw_len))?,
			);
			let total_len = packet_len.0 as usize + packet_len.count_bytes() as usize;

			if total_len > i32::MAX as usize {
				return Err(SerializerError::PacketTooLarge(total_len));
			}

			let mut out = Vec::with_capacity(total_len);
			out.extend(packet_len.to_bytes());
			out.extend(uncompressed_len.to_bytes());
			out.extend(compressed_data);

			Ok(out)
		} else {
			log::info!("Compressed and under threshold");
			let uncompressed_len = VarInt(0);
			let packet_len = VarInt(
				(raw_len + uncompressed_len.count_bytes() as usize)
					.try_into()
					.map_err(|_| SerializerError::PacketTooLarge(raw_len))?,
			);
			let total_len = packet_len.0 as usize + packet_len.count_bytes() as usize;

			if total_len > i32::MAX as usize {
				return Err(SerializerError::PacketTooLarge(total_len));
			}

			let mut out = Vec::with_capacity(total_len);
			out.extend(packet_len.to_bytes());
			out.extend(uncompressed_len.to_bytes());
			out.extend(raw);

			Ok(out)
		}
	}

	pub fn deserialize<T: PacketInfo>(&self) -> Result<T, DeserializerError> {
		if self.packet_id == T::PACKET_ID {
			T::deserialize(&mut PacketDeserializer::new(&self.data))
		} else {
			Err(DeserializerError::InvalidPacketId)
		}
	}

	pub fn serialize<T: PacketInfo>(data: &T) -> Result<Self, SerializerError> {
		let mut serializer = PacketSerializer::default();
		data.serialize(&mut serializer)?;
		Ok(Self { packet_id: T::PACKET_ID, data: serializer.take_output() })
	}

	pub fn finalize_raw(&self, packet_id: i32) -> Vec<u8> {
		let mut out = VarInt(packet_id).to_bytes();
		out.extend(&self.data);
		out
	}

	pub fn finalize_decompressed(&self) -> Result<Vec<u8>, SerializerError> {
		let packet_id = self.packet_id;
		let raw = self.finalize_raw(packet_id);
		let raw_len = raw.len();
		let mut output =
			VarInt(raw_len.try_into().map_err(|_| SerializerError::PacketTooLarge(raw_len))?)
				.to_bytes();
		output.extend(raw);
		Ok(output)
	}
}
