pub mod de;
pub mod packet;
pub mod ser;
pub mod varlen;

#[macro_export]
macro_rules! packet {
    ($name:ident, $id:literal, {
        $($lident:ident: $rtype:ty),*
    }) => {
        #[derive(Debug, serde::Deserialize, serde::Serialize, PartialEq)]
        pub struct $name {
            $(pub $lident: $rtype,)*
        }

        impl ::serde_mcpacket::packet::PacketInfo for $name {
            const PACKET_ID: i32 = $id;
        }
    };
}

#[cfg(test)]
mod tests {
	use std::io::Cursor;

	use serde::{Deserialize, Serialize};

	use crate::packet::{PacketInfo, RawPacket};

	#[derive(Debug, Deserialize, Serialize, PartialEq, Eq)]
	struct SimpleTestPacket {
		// 7 Bytes total
		arg_00: u16,
		arg_02: i32,
		arg_06: bool,
	}

	impl PacketInfo for SimpleTestPacket {
		const PACKET_ID: i32 = 3323;
	}

	impl Default for SimpleTestPacket {
		fn default() -> Self {
			Self { arg_00: 420, arg_02: -666, arg_06: false }
		}
	}

	const TEST_PACKET: &[u8] = &[
		0b0000_1001,
		0b1111_1011,
		0b0001_1001,
		0b0000_0001,
		0b1010_0100,
		0b1111_1111,
		0b1111_1111,
		0b1111_1101,
		0b0110_0110,
		0b0000_0000,
	];

	#[test]
	fn decompressed_serialization_test() {
		let test_packet = SimpleTestPacket::default();

		let res = crate::packet::RawPacket::serialize(&test_packet);
		assert!(res.is_ok());

		let raw_packet = res.unwrap();

		let res = raw_packet.finalize_decompressed();
		assert!(res.is_ok());

		let data = res.unwrap();
		assert_eq!(data.len(), 2 + 1 + 7);

		assert_eq!(TEST_PACKET.to_vec(), data);
	}

	#[test]
	fn decompressed_deserialize_test() {
		let res = RawPacket::from_read(&mut Cursor::new(TEST_PACKET), None);
		assert!(res.is_ok());

		let raw_packet = res.unwrap();

		let res: Result<SimpleTestPacket, _> = raw_packet.deserialize();
		assert!(res.is_ok());

		let packet = res.unwrap();
		assert_eq!(packet, SimpleTestPacket::default());
	}
}
