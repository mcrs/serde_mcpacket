use std::{convert::TryInto, fmt::Display, str::Utf8Error};

use serde::{
	de::{self, IntoDeserializer, MapAccess, SeqAccess},
	Deserializer,
};

use crate::varlen::{VarInt, VarLen, VAR_BITMASK};

#[derive(Debug)]
pub enum DeserializerError {
	/// De error
	Message(String),

	/// Reached end of buffer with more to deserialize
	EndOfBuffer,

	/// Value is not `0` or `1`
	InvalidBool,

	/// Packet ID mismatch
	InvalidPacketId,

	InvalidDataLength,
	InvalidStringLength,
	InvalidEnum,
	InvalidString(Utf8Error),
	IoError(std::io::Error),
}

impl std::error::Error for DeserializerError {}

impl de::Error for DeserializerError {
	fn custom<T>(msg: T) -> Self
	where
		T: Display,
	{
		Self::Message(msg.to_string())
	}
}

impl Display for DeserializerError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			DeserializerError::Message(content) => f.write_fmt(format_args!("{}", content)),
			_ => f.write_fmt(format_args!("{:?}", self)),
		}
	}
}

pub struct PacketDeserializer<'de> {
	pub data: &'de [u8],
	offset: usize,
	id: u32,
}

impl<'de> PacketDeserializer<'de> {
	pub fn new(data: &'de [u8]) -> Self {
		Self { data, offset: 0, id: 0 }
	}

	pub fn read(&mut self, size: usize) -> Result<&[u8], DeserializerError> {
		if size > self.remaining_bytes() {
			Err(DeserializerError::EndOfBuffer)
		} else {
			let old_offset = self.offset;
			self.offset += size;
			Ok(&self.data[old_offset..self.offset])
		}
	}

	pub fn remaining_bytes(&self) -> usize {
		self.data.len() - self.offset
	}

	pub fn read_varint(&mut self) -> VarInt {
		let out = VarInt::from_bytes(&self.data[self.offset..]);
		self.offset += out.count_bytes() as usize;
		out
	}

	pub fn parse_bool(&mut self) -> Result<bool, DeserializerError> {
		match self.read(1)?[0] {
			0x00 => Ok(false),
			0x01 => Ok(true),
			_ => Err(DeserializerError::InvalidBool),
		}
	}

	pub fn parse_str(&mut self) -> Result<&str, DeserializerError> {
		let len = self.read_varint();
		if len.0.is_negative() {
			return Err(DeserializerError::InvalidStringLength);
		}

		let len = len.0 as usize;

		std::str::from_utf8(self.read(len)?).map_err(DeserializerError::InvalidString)
	}
}

impl<'de, 'a> Deserializer<'de> for &'a mut PacketDeserializer<'de> {
	type Error = DeserializerError;

	fn deserialize_any<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		unimplemented!()
	}

	fn deserialize_bool<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		visitor.visit_bool(self.parse_bool()?)
	}

	fn deserialize_i8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		visitor.visit_i8(i8::from_be_bytes(self.read(1)?.try_into().unwrap()))
	}

	fn deserialize_i16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		visitor.visit_i16(i16::from_be_bytes(self.read(2)?.try_into().unwrap()))
	}

	fn deserialize_i32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		visitor.visit_i32(i32::from_be_bytes(self.read(4)?.try_into().unwrap()))
	}

	fn deserialize_i64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		visitor.visit_i64(i64::from_be_bytes(self.read(8)?.try_into().unwrap()))
	}

	fn deserialize_u8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		visitor.visit_u8(u8::from_be_bytes(self.read(1)?.try_into().unwrap()))
	}

	fn deserialize_u16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		visitor.visit_u16(u16::from_be_bytes(self.read(2)?.try_into().unwrap()))
	}

	fn deserialize_u32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		visitor.visit_u32(u32::from_be_bytes(self.read(4)?.try_into().unwrap()))
	}

	fn deserialize_u64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_u64(u64::from_be_bytes(self.read(8)?.try_into().unwrap()))
	}

	fn deserialize_f32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		visitor.visit_f32(f32::from_be_bytes(self.read(4)?.try_into().unwrap()))
	}

	fn deserialize_f64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		visitor.visit_f64(f64::from_be_bytes(self.read(8)?.try_into().unwrap()))
	}

	fn deserialize_char<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		visitor.visit_u8(u8::from_be_bytes(self.read(1)?.try_into().unwrap()))
	}

	fn deserialize_str<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		visitor.visit_str(self.parse_str()?)
	}

	fn deserialize_string<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		self.deserialize_str(visitor)
	}

	fn deserialize_bytes<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		unimplemented!()
	}

	fn deserialize_byte_buf<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		self.deserialize_bytes(visitor)
	}

	fn deserialize_option<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		unimplemented!()
	}

	fn deserialize_unit<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		visitor.visit_unit()
	}

	fn deserialize_unit_struct<V>(
		self,
		_name: &'static str,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		self.deserialize_unit(visitor)
	}

	fn deserialize_newtype_struct<V>(
		self,
		_name: &'static str,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		visitor.visit_newtype_struct(self)
	}

	fn deserialize_seq<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		let size = self.read_varint();

		if size.0.is_negative() {
			Err(DeserializerError::InvalidDataLength)
		} else if size.0 as usize > self.remaining_bytes() {
			Err(DeserializerError::EndOfBuffer)
		} else {
			visitor.visit_seq(&mut PacketAccessor::new(self, Some(size.0)))
		}
	}

	fn deserialize_tuple<V>(self, _len: usize, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		unimplemented!()
	}

	fn deserialize_tuple_struct<V>(
		self,
		_name: &'static str,
		_len: usize,
		_visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		unimplemented!()
	}

	fn deserialize_map<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		visitor.visit_map(PacketAccessor::new(self, None))
	}

	fn deserialize_struct<V>(
		self,
		_name: &'static str,
		_fields: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		self.deserialize_map(visitor)
	}

	fn deserialize_enum<V>(
		self,
		name: &'static str,
		variants: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		if name == "varint" {
			let bytecount = {
				let mut counter = 0;
				for x in &self.data[self.offset..(self.offset + 5).min(self.data.len())] {
					counter += 1;
					if *x & !VAR_BITMASK == 0 {
						break;
					}
				}
				counter
			};

			visitor.visit_bytes(self.read(bytecount)?)
		} else if name == "varlong" {
			let bytecount = {
				let mut counter = 0;
				for x in &self.data[self.offset..(self.offset + 9).min(self.data.len())] {
					counter += 1;
					if *x & !VAR_BITMASK == 0 {
						break;
					}
				}
				counter
			};

			visitor.visit_bytes(self.read(bytecount)?)
		} else {
			let varint =
				VarInt::from_bytes(&self.data[self.offset..(self.offset + 5).min(self.data.len())]);
			self.offset += varint.count_bytes() as usize;
			if varint.0.is_negative() || varint.0 as usize >= variants.len() {
				log::error!("Failed to read enum {}: {:?}", name, varint.0);
				Err(DeserializerError::InvalidEnum)
			} else {
				visitor.visit_enum(variants[varint.0 as usize].into_deserializer())
			}
		}
	}

	fn deserialize_identifier<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		let id = self.id;
		self.id += 1;
		visitor.visit_u32(id)
	}

	fn deserialize_ignored_any<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		unimplemented!()
	}

	fn is_human_readable(&self) -> bool {
		false
	}
}

struct PacketAccessor<'a, 'de: 'a> {
	de: &'a mut PacketDeserializer<'de>,
	remaining_elements: Option<i32>,
}

impl<'a, 'de> PacketAccessor<'a, 'de> {
	fn new(de: &'a mut PacketDeserializer<'de>, max_elements: Option<i32>) -> Self {
		Self { de, remaining_elements: max_elements }
	}
}

impl<'de, 'a> SeqAccess<'de> for PacketAccessor<'a, 'de> {
	type Error = DeserializerError;

	fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>, Self::Error>
	where
		T: de::DeserializeSeed<'de>,
	{
		if self.de.remaining_bytes() > 0 {
			if let Some(remaining_elements) = self.remaining_elements {
				if remaining_elements > 0 {
					let out = seed.deserialize(&mut *self.de).map(Some);
					if out.is_ok() {
						self.remaining_elements = Some(remaining_elements - 1);
					}
					return out;
				}
			}
		}
		Ok(None)
	}
}

impl<'de, 'a> MapAccess<'de> for PacketAccessor<'a, 'de> {
	type Error = DeserializerError;

	fn next_key_seed<K>(&mut self, seed: K) -> Result<Option<K::Value>, Self::Error>
	where
		K: de::DeserializeSeed<'de>,
	{
		if self.de.remaining_bytes() > 0 {
			seed.deserialize(&mut *self.de).map(Some)
		} else {
			Ok(None)
		}
	}

	fn next_value_seed<V>(&mut self, seed: V) -> Result<V::Value, Self::Error>
	where
		V: de::DeserializeSeed<'de>,
	{
		seed.deserialize(&mut *self.de)
	}
}
