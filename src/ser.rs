use std::{convert::TryInto, fmt::Display};

use serde::ser::{
	self, Serialize, SerializeMap, SerializeSeq, SerializeStruct, SerializeStructVariant,
	SerializeTuple, SerializeTupleStruct, SerializeTupleVariant,
};

use crate::varlen::{VarInt, VarLen};

#[derive(Debug)]
pub enum SerializerError {
	/// Ser error
	Message(String),

	/// usize is packet size in bytes
	PacketTooLarge(usize),

	/// usize is string size in bytes
	StringTooLarge(usize),

	/// usize is sequence length in T count
	SequenceTooLarge(usize),

	/// [char] gets casted to a u8, this is returned if the code-point is not
	/// valid ASCII
	CharNotAscii,

	/// Currently sequence where size is not known is not supported
	UnsizedSequence,

	/// Compression
	Compression(std::io::Error),
}

impl std::error::Error for SerializerError {}

impl ser::Error for SerializerError {
	fn custom<T>(msg: T) -> Self
	where
		T: Display,
	{
		Self::Message(msg.to_string())
	}
}

impl Display for SerializerError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			SerializerError::Message(content) => f.write_fmt(format_args!("{}", content)),
			_ => f.write_fmt(format_args!("{:?}", self)),
		}
	}
}

#[derive(Debug, Default)]
pub struct PacketSerializer {
	output: Vec<u8>,
}

impl PacketSerializer {
	pub fn take_output(self) -> Vec<u8> {
		self.output
	}
}

impl serde::Serializer for &mut PacketSerializer {
	type Ok = ();
	type Error = SerializerError;

	type SerializeSeq = Self;
	type SerializeTuple = Self;
	type SerializeTupleStruct = Self;
	type SerializeTupleVariant = Self;
	type SerializeMap = Self;
	type SerializeStruct = Self;
	type SerializeStructVariant = Self;

	fn serialize_bool(self, v: bool) -> Result<Self::Ok, Self::Error> {
		self.output.push(match v {
			true => 1,
			false => 0,
		});
		Ok(())
	}

	fn serialize_i8(self, v: i8) -> Result<Self::Ok, Self::Error> {
		(v as u8).serialize(self)
	}

	fn serialize_i16(self, v: i16) -> Result<Self::Ok, Self::Error> {
		v.to_be_bytes().serialize(self)
	}

	fn serialize_i32(self, v: i32) -> Result<Self::Ok, Self::Error> {
		v.to_be_bytes().serialize(self)
	}

	fn serialize_i64(self, v: i64) -> Result<Self::Ok, Self::Error> {
		v.to_be_bytes().serialize(self)
	}

	fn serialize_u8(self, v: u8) -> Result<Self::Ok, Self::Error> {
		self.output.push(v);
		Ok(())
	}

	fn serialize_u16(self, v: u16) -> Result<Self::Ok, Self::Error> {
		v.to_be_bytes().serialize(self)
	}

	fn serialize_u32(self, v: u32) -> Result<Self::Ok, Self::Error> {
		v.to_be_bytes().serialize(self)
	}

	fn serialize_u64(self, v: u64) -> Result<Self::Ok, Self::Error> {
		v.to_be_bytes().serialize(self)
	}

	fn serialize_f32(self, v: f32) -> Result<Self::Ok, Self::Error> {
		v.to_be_bytes().serialize(self)
	}

	fn serialize_f64(self, v: f64) -> Result<Self::Ok, Self::Error> {
		v.to_be_bytes().serialize(self)
	}

	fn serialize_char(self, v: char) -> Result<Self::Ok, Self::Error> {
		if v.is_ascii() {
			(v as u8).serialize(self)
		} else {
			Err(SerializerError::CharNotAscii)
		}
	}

	fn serialize_str(self, v: &str) -> Result<Self::Ok, Self::Error> {
		let bytes = v.as_bytes();
		let len_usize = bytes.len();
		let len_varint =
			VarInt(len_usize.try_into().map_err(|_| SerializerError::StringTooLarge(len_usize))?);

		self.output.extend(len_varint.to_bytes());
		self.output.extend(bytes);
		Ok(())
	}

	fn serialize_bytes(self, v: &[u8]) -> Result<Self::Ok, Self::Error> {
		self.output.extend(v);
		Ok(())
	}

	fn serialize_none(self) -> Result<Self::Ok, Self::Error> {
		Ok(())
	}

	fn serialize_some<T: ?Sized>(self, value: &T) -> Result<Self::Ok, Self::Error>
	where
		T: serde::Serialize,
	{
		value.serialize(self)
	}

	fn serialize_unit(self) -> Result<Self::Ok, Self::Error> {
		Ok(())
	}

	fn serialize_unit_struct(self, _name: &'static str) -> Result<Self::Ok, Self::Error> {
		self.serialize_unit()
	}

	fn serialize_unit_variant(
		self,
		_name: &'static str,
		variant_index: u32,
		_variant: &'static str,
	) -> Result<Self::Ok, Self::Error> {
		VarInt(variant_index as i32).serialize(self)
	}

	fn serialize_newtype_struct<T: ?Sized>(
		self,
		_name: &'static str,
		value: &T,
	) -> Result<Self::Ok, Self::Error>
	where
		T: serde::Serialize,
	{
		value.serialize(self)
	}

	fn serialize_newtype_variant<T: ?Sized>(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		value: &T,
	) -> Result<Self::Ok, Self::Error>
	where
		T: serde::Serialize,
	{
		value.serialize(self)
	}

	fn serialize_seq(self, len: Option<usize>) -> Result<Self::SerializeSeq, Self::Error> {
		match len {
			Some(len) => {
				let varint =
					VarInt(len.try_into().map_err(|_| SerializerError::SequenceTooLarge(len))?);
				self.output.extend(varint.to_bytes());
				Ok(self)
			}
			None => Err(SerializerError::UnsizedSequence),
		}
	}

	fn serialize_tuple(self, _len: usize) -> Result<Self::SerializeTuple, Self::Error> {
		Ok(self)
	}

	fn serialize_tuple_struct(
		self,
		_name: &'static str,
		_len: usize,
	) -> Result<Self::SerializeTupleStruct, Self::Error> {
		Ok(self)
	}

	fn serialize_tuple_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_len: usize,
	) -> Result<Self::SerializeTupleVariant, Self::Error> {
		Ok(self)
	}

	fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap, Self::Error> {
		Ok(self)
	}

	fn serialize_struct(
		self,
		_name: &'static str,
		_len: usize,
	) -> Result<Self::SerializeStruct, Self::Error> {
		Ok(self)
	}

	fn serialize_struct_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_len: usize,
	) -> Result<Self::SerializeStructVariant, Self::Error> {
		Ok(self)
	}
}

impl SerializeSeq for &mut PacketSerializer {
	type Ok = ();

	type Error = SerializerError;

	fn serialize_element<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
	where
		T: serde::Serialize,
	{
		value.serialize(&mut **self)
	}

	fn end(self) -> Result<Self::Ok, Self::Error> {
		Ok(())
	}
}

impl SerializeTuple for &mut PacketSerializer {
	type Ok = ();

	type Error = SerializerError;

	fn serialize_element<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
	where
		T: serde::Serialize,
	{
		value.serialize(&mut **self)
	}

	fn end(self) -> Result<Self::Ok, Self::Error> {
		Ok(())
	}
}

impl SerializeTupleStruct for &mut PacketSerializer {
	type Ok = ();

	type Error = SerializerError;

	fn serialize_field<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
	where
		T: serde::Serialize,
	{
		value.serialize(&mut **self)
	}

	fn end(self) -> Result<Self::Ok, Self::Error> {
		Ok(())
	}
}

impl SerializeTupleVariant for &mut PacketSerializer {
	type Ok = ();

	type Error = SerializerError;

	fn serialize_field<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
	where
		T: serde::Serialize,
	{
		value.serialize(&mut **self)
	}

	fn end(self) -> Result<Self::Ok, Self::Error> {
		Ok(())
	}
}

impl SerializeMap for &mut PacketSerializer {
	type Ok = ();

	type Error = SerializerError;

	fn serialize_key<T: ?Sized>(&mut self, _key: &T) -> Result<(), Self::Error>
	where
		T: serde::Serialize,
	{
		Ok(())
	}

	fn serialize_value<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
	where
		T: serde::Serialize,
	{
		value.serialize(&mut **self)
	}

	fn end(self) -> Result<Self::Ok, Self::Error> {
		Ok(())
	}
}

impl SerializeStruct for &mut PacketSerializer {
	type Ok = ();

	type Error = SerializerError;

	fn serialize_field<T: ?Sized>(
		&mut self,
		_key: &'static str,
		value: &T,
	) -> Result<(), Self::Error>
	where
		T: serde::Serialize,
	{
		value.serialize(&mut **self)
	}

	fn end(self) -> Result<Self::Ok, Self::Error> {
		Ok(())
	}
}

impl SerializeStructVariant for &mut PacketSerializer {
	type Ok = ();

	type Error = SerializerError;

	fn serialize_field<T: ?Sized>(
		&mut self,
		_key: &'static str,
		value: &T,
	) -> Result<(), Self::Error>
	where
		T: serde::Serialize,
	{
		value.serialize(&mut **self)
	}

	fn end(self) -> Result<Self::Ok, Self::Error> {
		Ok(())
	}
}
