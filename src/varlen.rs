use serde::{
	de::{Error, Visitor},
	Deserialize, Serialize,
};
use tokio::io::AsyncReadExt;

pub(crate) const VAR_BITMASK: u8 = 0b0111_1111;

#[derive(Debug, PartialEq, PartialOrd)]
pub struct VarInt(pub i32);
#[derive(Debug, PartialEq, PartialOrd)]
pub struct VarLong(pub i64);

#[async_trait::async_trait]
pub trait VarLen: Sized {
	fn to_bytes(&self) -> Vec<u8>;
	async fn from_async_reader<R: tokio::io::AsyncRead + Send + Sync + Unpin>(
		reader: &mut R,
	) -> Result<Self, tokio::io::Error>;
	fn from_reader<R: std::io::Read + Send + Sync + Unpin>(
		reader: &mut R,
	) -> Result<Self, std::io::Error>;
	fn from_bytes(bytes: &[u8]) -> Self;
	fn count_bytes(&self) -> i32;
}

#[async_trait::async_trait]
impl VarLen for VarInt {
	fn to_bytes(&self) -> Vec<u8> {
		let mut buffer = Vec::new();
		let mut val = self.0;
		for x in 0..5 {
			buffer.push(val as u8 & VAR_BITMASK);
			val >>= 7;
			if val != 0 {
				buffer[x] |= !VAR_BITMASK;
			} else {
				break;
			}
		}
		buffer
	}

	async fn from_async_reader<R: tokio::io::AsyncRead + Send + Sync + Unpin>(
		reader: &mut R,
	) -> Result<Self, tokio::io::Error> {
		let mut buffer = [0u8; 5];

		for x in 0..5 {
			reader.read_exact(&mut buffer[x..x + 1]).await?;

			if buffer[x] & !VAR_BITMASK == 0 {
				break;
			}
		}

		Ok(Self::from_bytes(&buffer))
	}

	fn from_reader<R: std::io::Read + Send + Sync + Unpin>(
		reader: &mut R,
	) -> Result<Self, std::io::Error> {
		let mut buffer = [0u8; 5];

		for x in 0..5 {
			reader.read_exact(&mut buffer[x..x + 1])?;

			if buffer[x] & !VAR_BITMASK == 0 {
				break;
			}
		}

		Ok(Self::from_bytes(&buffer))
	}

	fn from_bytes(bytes: &[u8]) -> Self {
		let mut out = 0;

		for (idx, x) in bytes.iter().enumerate().take(5) {
			out |= ((*x & VAR_BITMASK) as i32) << (idx * 7);

			if *x & !VAR_BITMASK == 0 {
				break;
			}
		}

		Self(out)
	}

	fn count_bytes(&self) -> i32 {
		let mut remaining = self.0 >> 7;
		let mut bytes = 1;

		while remaining != 0 {
			bytes += 1;
			remaining >>= 7;
		}

		bytes
	}
}

#[async_trait::async_trait]
impl VarLen for VarLong {
	fn to_bytes(&self) -> Vec<u8> {
		let mut buffer = Vec::new();
		let mut val = self.0;
		for x in 0..9 {
			buffer.push(val as u8 & VAR_BITMASK);
			val >>= 7;
			if val != 0 {
				buffer[x] |= !VAR_BITMASK;
			} else {
				break;
			}
		}
		buffer
	}

	async fn from_async_reader<R: tokio::io::AsyncRead + Send + Sync + Unpin>(
		reader: &mut R,
	) -> Result<Self, tokio::io::Error> {
		let mut buffer = [0u8; 9];

		for x in 0..9 {
			reader.read_exact(&mut buffer[x..x + 1]).await?;

			if buffer[x] & !VAR_BITMASK == 0 {
				break;
			}
		}

		Ok(Self::from_bytes(&buffer))
	}

	fn from_reader<R: std::io::Read + Send + Sync + Unpin>(
		reader: &mut R,
	) -> Result<Self, std::io::Error> {
		let mut buffer = [0u8; 9];

		for x in 0..9 {
			reader.read_exact(&mut buffer[x..x + 1])?;

			if buffer[x] & !VAR_BITMASK == 0 {
				break;
			}
		}

		Ok(Self::from_bytes(&buffer))
	}

	fn from_bytes(bytes: &[u8]) -> Self {
		let mut out = 0;

		for (idx, x) in bytes.iter().enumerate().take(9) {
			out |= ((*x & VAR_BITMASK) as i64) << (idx * 7);

			if *x & !VAR_BITMASK == 0 {
				break;
			}
		}

		Self(out)
	}

	fn count_bytes(&self) -> i32 {
		let mut remaining = self.0 >> 7;
		let mut bytes = 1;

		while remaining != 0 {
			bytes += 1;
			remaining >>= 7;
		}

		bytes
	}
}

struct VarIntVisitor;
struct VarLongVisitor;

impl<'de> Visitor<'de> for VarIntVisitor {
	type Value = VarInt;

	fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
		formatter.write_str("a varint")
	}

	fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
	where
		E: Error,
	{
		Ok(VarInt::from_bytes(v))
	}
}

impl<'de> Visitor<'de> for VarLongVisitor {
	type Value = VarLong;

	fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
		formatter.write_str("a varlong")
	}
	fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
	where
		E: Error,
	{
		Ok(VarLong::from_bytes(v))
	}
}

impl<'de> Deserialize<'de> for VarInt {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		deserializer.deserialize_enum("varint", &[], VarIntVisitor)
	}
}

impl<'de> Deserialize<'de> for VarLong {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		deserializer.deserialize_enum("varlong", &[], VarLongVisitor)
	}
}

impl Serialize for VarInt {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::Serializer,
	{
		self.to_bytes().as_slice().serialize(serializer)
	}
}

impl Serialize for VarLong {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::Serializer,
	{
		self.to_bytes().as_slice().serialize(serializer)
	}
}

#[cfg(test)]
mod tests {
	use super::{VarInt, VarLen};

	#[test]
	fn serialization() {
		assert_eq!(VarInt(666).to_bytes(), vec![0b1001_1010, 0b101])
	}

	#[test]
	fn deserialization() {
		assert_eq!(VarInt::from_bytes(&[0b1001_1010, 0b101]), VarInt(666))
	}

	#[test]
	fn byte_count() {
		assert_eq!(VarInt(666).count_bytes(), 2)
	}
}
